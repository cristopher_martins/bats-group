<?php get_header(); ?>

  
  <section id="no-results" class="padding-content">
    <div class="uk-container uk-container-center">
      <h1 class="title-sections">
        <?php _e( 'Erro 404', 'bats' );?>
      </h1>
    </div>
                    <div class="uk-container uk-container-center uk-text-center">
                              
                              <h2 class="subtitle-sections">
                                <?php _e( 'Página não encontrada', 'bats' );?>
                                <br/><br/><i class="uk-icon-thumbs-down uk-icon-large"></i>
                              </h2>
                              <p>
                                <?php _e( 'Desculpe, A página que você esta tentando acessar não existe ou não esta mais disponível!', 'bats' );?>
                              </p>
                              <div class="">
                                <a href="<?php bloginfo('url');?>" class="uk-button uk-button-primary">
                                  <?php _e( 'Voltar para home', 'bats' );?>
                                </a>
                              </div>
                    </div>
                  
  </section>

<?php get_footer(); ?>