<?php get_header( );?>
<section id="cases" class="padding-content opacity-back">
      <div class="uk-container uk-container-center">
        <h2 class="title-sections">
          <?php single_cat_title( );?>
        </h2>
      </div>
      <!-- Filter Controls -->
      <div class="container-tabs">
        <div class="uk-container uk-container-center">
          
            <?php $tags = get_tags();
            $html = '<ul id="cases" class="uk-subnav tabnav-padrao">';
            $html .= '<li class="uk-active" data-uk-filter=""><a href="">';
            $html .= __( 'Todos', 'bats' ).'</a></li>';
            foreach ( $tags as $tag ) {
              $tag_link = get_tag_link( $tag->term_id );
                  
              $html .= "<li data-uk-filter='{$tag->slug}'><a href='' title='{$tag->name}'>";
              $html .= "{$tag->name}</a></li>";
            }
            $html .= '</ul>';
            echo $html;?>
          
        </div>
      </div>

      <!-- Dynamic Grid -->
      <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-4" data-uk-grid="{controls: '#cases'}">
          
          
           <?php query_posts('category_name=cases'); ?>
            <?php while (have_posts()) : the_post(); ?>
              <a href="<?php the_permalink();?>" data-uk-filter=" 
              <?php $posttags = get_the_tags();
                if ($posttags) {
                  foreach($posttags as $tag) {
                    echo $tag->slug . ' ', ','; 
                  }
                } ?> ">
                <article class="case-box">
                  <?php the_post_thumbnail( );?>
                  <div class="mask-case">
                    <div class="container-case">
                      <h3 class="min-title">
                        <?php the_title();?>
                      </h3>
                      <?php the_excerpt();?>
                    </div>
                  </div>
                </article>
              </a>
            <?php endwhile; ?>
      </div>
    </section>
<? get_footer( );?>
