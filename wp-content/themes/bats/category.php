<?php get_header( );?>
<div class="master_banner-single">
    <div class="uk-container uk-container-center"> 
      <div class="padding-content">
        <h1 class="title-sections">
          <?php single_cat_title(); ?>
        </h1>
      </div>
    </div>
  </div>
  <div class="uk-container uk-container-center">
  <div class="uk-grid uk-grid-divider">
    <main id="blog" class="uk-width-large-3-4">
      <section class="uk-grid container-blog-list">
            <!-- Start the Loop. -->
              <?php while (have_posts()) : the_post(); ?>

              <div class="uk-width-large-1-3 uk-width-medium-1-2">
                <article class="post-block-cat">
                  <a href="<?php the_permalink();?>">
                  <?php the_post_thumbnail('cat-thumb');?>
                  </a>
                  <div class="cat-list-content">
                    <h2>
                      <?php the_title();?>
                    </h2>
                    <p class="small-metas">
                      <?php the_time('j F Y') ?> &nbsp;|&nbsp; 
                      <!-- by <?php the_author() ?> -->
                      <?php _e( 'Publicado em', 'bats' );?>
                      <?php the_category(', ');
                        if($post->comment_count > 0) { 
                            echo ' &nbsp;|&nbsp; ';
                            comments_popup_link('', '1 Comment', '% Comments'); 
                        }
                      ?>
                    </p>
                    <a href="<?php the_permalink();?>" class="uk-button uk-button-small uk-button-primary">
                      <?php _e( 'Leia Mais', 'bats' );?>
                    </a>
                  </div>
                </article>
              </div>
             <?php endwhile; ?>
      </section>
    </main>
    <sidebar class="uk-width-large-1-4">
      <div class="container-sidebar">
        <?php dynamic_sidebar('blog-sidebar'); ?>
      </div>
    </sidebar>
  </div>
  </div>
<? get_footer( );?>
