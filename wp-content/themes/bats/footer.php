
<footer class="yellow-back padding-content">
	<div class="uk-container uk-container-center uk-text-center">
		<div class="menu-rdp uk-text-cnter">
			<?php wp_nav_menu( array( 'theme_location' => 'menu-topo', 'container_class' => 'menu-content') ); ?>
		</div>
		<div class="uk-text-center">
			<img src="<?php bloginfo('template_directory');?>/assets/imagens/bats-logo-rdp.png">
		</div>
		<div class="menu-companies uk-text-center">
			<?php query_posts('category_name=empresas&order=ASC'); ?>
			<ul>
            <?php while (have_posts()) : the_post(); ?>
                <li>
                	<a href="<?php the_permalink();?>">
						<?php the_title();?>
                	</a>
                </li>
            <?php endwhile; ?>
            </ul>
		</div>
	</div>
	<div class="foot-rdp">
		<div class="uk-container uk-container-center">
			<div class="uk-grid">
				<div class="uk-width-large-1-4 logo-rdp">
					<img width="100" src="<?php bloginfo('template_directory');?>/assets/imagens/logo-black.svg">
				</div>
				<div class="uk-width-large-2-4 uk-text-center copyright-rdp"><?php _e( '©2017 Bats group – todos os direitos reservados', 'bats' );?></div>
				<div class="uk-width-large-1-4 uk-text-right">
					<ul class="social-rdp">
						<li>
							<a href="https://www.facebook.com/grupobats/" class="uk-icon-facebook"></a>
							<a href="https://twitter.com/grupobats" class="uk-icon-twitter"></a>
							<a href="https://www.instagram.com/grupobats/" class="uk-icon-instagram"></a>
							<a href="https://www.youtube.com/channel/UCOAouZmbh0uUuELld7svM4A" class="uk-icon-youtube"></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer();?>
</body>
</html>