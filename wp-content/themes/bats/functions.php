<?php

date_default_timezone_set('America/Sao_Paulo');

/**
 * Theme initialization
 */
require get_parent_theme_file_path( '/inc/init.php' );

/**
* Theme Post types
*/
// require get_parent_theme_file_path( '/inc/custom-post-types.php' );

/**
* Theme Helpers
*/
require get_parent_theme_file_path( '/inc/helpers.php' );

/**
* Lista de cadastros para newsletter
*/
// require get_parent_theme_file_path( '/inc/admin-newsletter.php' );