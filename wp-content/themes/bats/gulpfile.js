var	gulp       = require('gulp'),
	concat     = require('gulp-concat'),
	uglify     = require('gulp-uglify'),
	rename     = require('gulp-rename'),
	sass       = require('gulp-sass'),
	bless      = require('gulp-bless'),
	livereload = require('gulp-livereload'),
	gulpUtil   = require('gulp-util');

var assetsFolder = './assets/';

var config = {
	scripts: [
		'node_modules/uikit/dist/js/uikit.js',
		'node_modules/uikit/dist/js/components/slideshow.js',
		'node_modules/uikit/dist/js/components/slideshow-fx.js',
		'node_modules/uikit/dist/js/components/grid.js',
		'node_modules/uikit/dist/js/components/sticky.js',
		'assets/js/app/*'
	],
	sass: [
		(assetsFolder + 'sass/**/*.scss')
	],
	sassDest: (assetsFolder + 'css/'),
	scriptDest: (assetsFolder + 'js/')
};

gulp.task('scripts', function() {
	return gulp.src(config.scripts)
		.pipe(uglify().on('error', function(error) {
			gulpUtil.log(
				error.toString()
				.replace(/\/.*?\/(themes)\/.+?\//gi,'')
				.replace(/: syntaxerror/gi,':\n\t'+gulpUtil.colors.red.bold.underline('SyntaxError'))
				.replace(/(filename:.+\/)(.+)/gi,'$1'+gulpUtil.colors.magenta.bold.underline('$2'))
				.replace(/(linenumber: )(\d+)/gi,'$1'+gulpUtil.colors.red.bold.underline('$2'))
			);
			this.emit('end');
		}))
		.pipe(concat('app.min.js'))
		.pipe(gulp.dest(config.scriptDest))
		.pipe(livereload());
});

gulp.task('sass', function () {
	return gulp.src(assetsFolder+'sass/style.scss')
		.pipe(sass.sync({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(gulp.dest(config.sassDest));
});

gulp.task('minify-css', ['sass'], function () {
	return gulp.src([
			'node_modules/font-awesome/css/font-awesome.css',
			'node_modules/uikit/dist/css/uikit.css',
			'node_modules/uikit/dist/css/components/slideshow.css',
			'node_modules/uikit/dist/css/components/dotnav.css',
			'node_modules/uikit/dist/css/components/slidenav.css',
			assetsFolder+'css/style.css',
			'!'+assetsFolder+'css/all*min*.css'])
		.pipe(concat('all.min.css'))
		.pipe(bless())
		.pipe(sass.sync({outputStyle: 'compressed'}).on('error', sass.logError))
		.pipe(gulp.dest(config.sassDest))
		.pipe(livereload());
});

gulp.task('watch', function () {
	livereload.listen(35729);
	gulp.watch('./*.php').on('change', function(file) {
		livereload.changed(file.path);
	});
	gulp.watch('./*.html').on('change', function(file) {
		livereload.changed(file.path);
	});
	gulp.watch(config.sass, ['sass','minify-css']);
	gulp.watch(assetsFolder+'js/*/*.js', ['scripts']);
});

gulp.task('copy', function() {
	gulp.src([
		'node_modules/font-awesome/fonts/**',
		'node_modules/lightgallery/dist/fonts/**',
	]).pipe(gulp.dest("assets/fonts/"));

});

gulp.task('default', ['copy', 'sass', 'minify-css', 'scripts', 'watch']);
