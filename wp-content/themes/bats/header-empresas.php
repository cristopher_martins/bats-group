<!DOCTYPE html>
<html lang="pt-BR">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php bloginfo("name"); ?><?php echo wp_title(" | "); ?></title>
	<?php wp_head(); ?>
	<script>
		var $buoop = {vs:{i:9,f:-4,o:-4,s:8,c:-4},unsecure:true,api:4};
		function $buo_f(){
		 var e = document.createElement("script");
		 e.src = "//browser-update.org/update.min.js";
		 document.body.appendChild(e);
		};
		try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
		catch(e){window.attachEvent("onload", $buo_f)}
	</script>
</head>

<body class="black-theme">

<header class="master-header" data-uk-sticky="{top:-400, animation: 'uk-animation-slide-top'}">
	<div class="uk-container uk-container-center">

		<nav>
		    <a href="<?php the_field('link_empresa');?>" class="uk-navbar-brand logo-top-empresas logo-top">
				<?php the_field('logo_svg_code');?>
			</a>
		    <div class="uk-navbar-flip menu-top uk-hidden-small">
		    	<div class="menu-content">
			    	<ul class="menu">
			    		<li>
			    			<a href="<?php bloginfo('url');?>">
				    			<?php _e( 'Voltar para o Bats', 'bats' );?>
				    		</a>
			    		</li>
			    		<li>
			    			<a href="<?php the_field('link_empresa');?>" class="uk-button uk-button-small">
				    			<?php _e( 'Visitar o site', 'bats' );?>
				    		</a>
			    		</li>
			    	</ul>
		    	</div>
		    		
					<div class="theme-switcher">
						<span></span>
					</div>
					<!-- <a class="uk-icon-search icone-busca-menu" href="#busca" data-uk-modal="{center:true}">
					</a> -->
					<?php echo qtranxf_generateLanguageSelectorShortcode(); ?>
			</div>
			<div class="uk-navbar-flip menu-top uk-visible-small drop-mobile">
				<a class="uk-icon-bars" href="#mobile" data-uk-offcanvas="{mode:'slide'}">
				</a>
			</div>
		</nav>
	</div>

</header>

<div id="mobile" class="uk-offcanvas menu-mobile">
    <div class="uk-offcanvas-bar menu-top">
		<div class="menu-content">
			    	<ul class="menu">
			    		<li>
			    			<a href="<?php bloginfo('url');?>">
				    			<?php _e( 'Voltar para o Bats', 'bats' );?>
				    		</a>
			    		</li>
			    		<li>
			    			<a href="<?php the_field('link_empresa');?>" class="uk-button uk-button-small">
				    			<?php _e( 'Visitar o site', 'bats' );?>
				    		</a>
			    		</li>
			    	</ul>
		    	</div>
		    		
					<div class="theme-switcher">
						<span></span>
					</div>
					<!-- <a class="uk-icon-search icone-busca-menu" href="#busca" data-uk-modal="{center:true}">
					</a> -->
					<?php echo qtranxf_generateLanguageSelectorShortcode(); ?>
	    </div>
	</div>

<div id="busca" class="uk-modal modal-busca">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <?php get_search_form(); ?>
    </div>
</div>


