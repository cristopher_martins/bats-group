<?php
/**
 * Language Selector Shortcode
*/
function qtranxf_generateLanguageSelectorShortcode() {
	global $q_config;

	if(is_404()) $url = get_option('home'); else $url = '';

	echo PHP_EOL.'<div class="lang-sel idioma-menu sel-dropdown"><ul>'.PHP_EOL;
        foreach(qtranxf_getSortedLanguages() as $language) {
                $alt = $q_config['language_name'][$language].' ('.$language.')';
                $classes = array('lang-'.$language);
                if($language == $q_config['language']) $classes[] = 'active';
                echo '<li class="'. implode(' ', $classes) .'"><a href="'.qtranxf_convertURL($url, $language, false, true).'"';
                // set hreflang
                echo ' hreflang="'.$language.'"';
                echo ' title="'.$alt.'"';
                echo ' >';
                echo $language;
                echo '</a></li>'.PHP_EOL;
        }
	echo '</ul></div><div class="qtranxs_widget_end"></div>'.PHP_EOL;
}
add_shortcode('qtranslate_selector', 'qtranxf_generateLanguageSelectorShortcode'); 