<?php
/**
 * Set the content width based on the theme's design and stylesheet.
 */
load_theme_textdomain('bats', get_template_directory() . '/languages');
if ( ! isset( $content_width ) ) {
	$content_width = 1230; /* pixels */
}
function my_function_admin_bar(){
    return false;
}
add_filter( 'show_admin_bar' , 'my_function_admin_bar');

if ( ! function_exists( 'bats_setup' ) ) :
function bats_setup() {
	// Clean up the head
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'wp_generator' );
	remove_action( 'wp_head', 'wp_shortlink_wp_head' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'title-tag' );

	// Register nav menus
	register_nav_menus( array(
		'menu-topo' => __( 'Menu Topo', 'bats' )
	) );

	// Add Editor Style
	add_editor_style();

	/* Prevent File Modifications
	if ( ! defined( 'DISALLOW_FILE_EDIT' ) ) {
		define( 'DISALLOW_FILE_EDIT', true );
	} */

	// Enable support for Post Thumbnails on posts and pages.
	add_theme_support( 'post-thumbnails' );

	// Add Image Sizes
	// add_image_size( $name, $width = 0, $height = 0, $crop = false );
	add_image_size( 'blog-thumb', '100', '100', true );
	add_image_size( 'cat-thumb', '380', '220', false );
	add_image_size( 'single-thumb', '600', '380', false );

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	
}
endif; // bats_setup

add_action( 'after_setup_theme', 'bats_setup' );

if( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page(array(
		'page_title' => 'Opções Gerais',
		'menu_title' => 'Opções Gerais',
		'menu_slug'  => 'theme-general-settings',
		'capability' => 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title'  => 'Configurações da Home',
		'menu_title'  => 'Home',
		'parent_slug' => 'theme-general-settings',
	));
}

function bats_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'bats' ),
		'id'            => 'blog-sidebar',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'bats' ),
		'before_widget' => '<div class="blog-sidebar-adv">',
		'after_widget'  => '</div>'
	) );
}
add_action( 'widgets_init', 'bats_widgets_init' );

// Assets
function theme_assets() {
	global $wp_query;

	# CSS
	wp_enqueue_style ( 'google-fonts', 'https://fonts.googleapis.com/css?family=Crimson+Text:400,400i|Lato:300,400,700' );
	wp_enqueue_style ( 'main', get_template_directory_uri() . '/assets/css/all.min.css' );

	# JS
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBISNJEq4iYy2ZIFIWOpJ0MR5qrwk6jrMQ' );
	wp_enqueue_script( 'js', get_template_directory_uri() . '/assets/js/app.min.js' );
	

	wp_localize_script(
		'jquery',
		'paprica',
		array(
			'template'       => get_bloginfo('template_url'),
			'url'            => get_bloginfo('url'),
			'ajaxurl'        => admin_url( 'admin-ajax.php' ),
			'current_page'   => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
			'max_page'       => $wp_query->max_num_pages,
			'posts_per_page' => get_query_var( 'posts_per_page' ),
			'query_vars'     => json_encode( $wp_query->query_vars )
		)
	);
}
add_action( 'wp_enqueue_scripts', 'theme_assets' );