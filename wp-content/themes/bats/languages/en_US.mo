��    !      $  /   ,      �  
  �     �     �  �   �     �     �  �   �  ^   m     �     �     �  z   �  	   u  �        A  y   W  �   �  	   �  
   �  8   �     �     �     �  
   	  	   	     	     #	     )	     /	     >	     O	  2   b	  �  �	  �   �     7     <  �   C     �     �  �   �  T   Y     �  	   �     �  b   �  	   ?  �   I     �  n   �  �   ]     �       0   
     ;     H     W     c     l     u     {       
   �     �     �  )   �     !                              	                                                                                            
                                BATS <strong>(Business Association in Tech Solutions)</strong>, morcegos em inglês, morcegos vampiros são
                animais que trabalham em equipe para buscar alimentos e assim
                dividir com  o restante do grupo para garantir a sobrevivência. Blog Busca Cadastre seus dados preenchendo os campos abaixo:
              <span>Os campos marcados com “<strong> • </strong>” são de preenchimento obrigatório.</span> Cases Contato Descreva o que precisa em poucas palavras preenchendo os campos abaixo:
              <span>Os campos marcados com “<strong> • </strong>” são de preenchimento obrigatório.</span> Desculpe, A página que você esta tentando acessar não existe ou não esta mais disponível! Empresas do grupo Erro 404 Faça sua Busca... Focamos em criar produtos que possam trazer a melhor experiência e melhor resultado para nossos
                clientes. Leia Mais Mande-nos uma mensagem ou solicite um orçamento preenchendo os campos abaixo:
              <span>Os campos marcados com “<strong> • </strong>” são de preenchimento obrigatório.</span> Nenhum resultado para Nossa missão é proporcionar o melhor resultado utilizando a união e expertise de cada empresa como nosso maior trunfo. O grupo surgiu com a união de quatro empresas especializadas em diferentes áreas unidos para atender itegralmente nossos clientes com o que cada empresa pode oferecer de melhor. O projeto Orçamento Por favor, tente uma busca diferente ou volte para home. Publicado em Página não encontrada Resultados para Saiba Mais Serviços Sobre Todos Vagas Visitar o site Voltar para home Voltar para o Bats ©2017 bats group – todos os direitos reservados Project-Id-Version: bats
POT-Creation-Date: 2017-10-04 13:49-0300
PO-Revision-Date: 2017-10-04 13:49-0300
Last-Translator: 
Language-Team: www.bats.com.br
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.0.4
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: _e;__;esc_html_e
X-Poedit-Basepath: ..
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: node_modules
 BATS <strong> (Business Association in Tech Solutions) </strong>, vampire bats are animals that work in teams to get food and so split with the rest of the group to ensure survival. Blog Search Register your data by filling in the fields below: <span> The fields marked with “<strong> • </strong>” are required. </span> Cases Contact Register your data by filling in the fields below: <span> The fields marked with “<strong> • </strong>” are required. </span> Sorry, the page you are trying to access does not exist or is not available anymore! Group companies 404 Error Type your search… We focus on creating products that can bring the best experience and best result to our customers. Read More Send us a message or request a quote by filling in the fields below: <span> The fields marked with “<strong> • </strong>” are required. </span> No results for Our mission is to provide the best result using the union and expertise of each company as our greatest asset. The group came up with the union of four companies specialized in different areas united to serve our customers with what each company can offer the best. The project Quote Please try a different search or return to home. Published in Page not found Results for See More Services About All Apply Visit Site Back to home page Back to Bats ©2017 bats group – all rights reserved 