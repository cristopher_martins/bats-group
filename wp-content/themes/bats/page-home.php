<?php get_header( );?>

    <?php if( have_rows('banner') ): ?>
 
      <div id="home" class="master_banner">
      <div class="uk-slidenav-position" data-uk-slideshow>
        <ul class="uk-slideshow uk-slideshow-fullscreen uk-overlay-active">
 
            <?php while ( have_rows('banner') ) : the_row(); ?>   
 
                <li>
                  <img src="<?php the_sub_field('imagem_background');?>">
                  <div class="uk-overlay-panel uk-overlay-fade uk-flex uk-flex-middle">
                    <div class="uk-container uk-container-center">
                      <div class="uk-grid">
                        <div class="uk-width-large-1-2 uk-text-center">
                          <img src="<?php the_sub_field('imagem_destaque');?>">
                        </div>
                        <div class="uk-width-large-1-2 text-banner">
                          <?php $post_object = get_sub_field('post'); ?>
 
                          <?php if( $post_object ): ?>
       
                              <?php $post = $post_object; setup_postdata( $post ); ?>

                              <?php the_category();?>
                              <h2>
                                <?php the_excerpt(); ?>
                              </h2>
                              <a href="<?php the_permalink(); ?>" class="uk-margin-top uk-button uk-button-primary"><?php _e( 'Saiba Mais', 'bats' );?></a>
       
                              <?php wp_reset_postdata(); ?>
       
                          <?php endif; ?>
                        
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
            <?php endwhile; ?>
        </ul>
        <ul class="uk-dotnav uk-dotnav-contrast uk-position-bottom uk-flex-center">
          <?php $i = 0; while ( have_rows('banner') ) : the_row(); ?>   
            <li data-uk-slideshow-item="<?php echo $i; ?>"><a href=""></a></li>
          <?php $i++; endwhile; ?>
        </ul>
    </div>
    </div>
    <?php endif; ?>

    <section id="sobre" class="padding-content">
      <div class="uk-container uk-container-center">
          <h2 class="title-sections"><?php _e( 'Sobre', 'bats' );?></h2>
          <div class="uk-grid">
            <div class="uk-width-large-1-3">
              <p>
                <?php _e( 'BATS <strong>(Business Association in Tech Solutions)</strong> assim como morcegos em inglês. Morcegos vampiros são
                animais que trabalham em equipe para buscar alimentos e assim dividir com  o restante do grupo para garantir a sobrevivência.', 'bats' );?>

              </p>
              <p>
                <?php _e( 'O grupo surgiu com a união de quatro empresas especializadas em diferentes áreas da tecnologia unidas para atender integralmente nossos clientes com o que cada empresa pode oferecer de melhor.', 'bats' );?>
              </p>
            </div>
            <div class="uk-width-large-1-3">
              <img src="<?php bloginfo('template_directory');?>/assets/imagens/business-association-in-tech-solutions.png">
              <h3 class="significado-bats">
                Business Association in Tech Solutions
              </h3>
            </div>
            <div class="uk-width-large-1-3">
              <p>
                <?php _e( 'Nossa missão é proporcionar o melhor resultado utilizando a união e expertise de cada empresa como nosso maior trunfo.', 'bats' );?>
                
              </p>
              <p>
                <?php _e( 'Focamos em criar produtos que possam trazer a melhor experiência e melhor resultado para nossos
                clientes.', 'bats' );?>
                
              </p>
            </div>
        </div>
      </div>

        <div class="companies-content padding-content">
          <div class="uk-container uk-container-center">
            <div class="uk-grid">
              <h3 class="uk-width-1-1 subtitle-sections uk-text-center"><?php _e( 'Empresas do grupo', 'bats' );?></h3>

              <?php query_posts('category_name=empresas&order=ASC'); ?>
              <?php while (have_posts()) : the_post(); ?>
                <div class="uk-width-large-1-2">
                  <div class="company-block" link_company="<?php the_permalink();?>">
                    <?php the_post_thumbnail( );?>
                    
                    <?php the_content();?>

                    <a class="link_companies" href="<?php the_permalink();?>">
                    </a>
                  </div>
                </div>
               <?php endwhile; ?>

            </div>
          </div>
        </div>
    </section>

    <section id="servicos" class="padding-content">
      <div class="uk-container uk-container-center">
          <h2 class="title-sections"><?php _e( 'Serviços', 'bats' );?></h2>
          <div class="uk-grid">

            <?php query_posts('category_name=servicos&order=ASC'); ?>
            <?php while (have_posts()) : the_post(); ?>
              <div class="uk-width-large-1-4 uk-container-center">
                <div class="blocks-services">
                  <?php the_post_thumbnail( );?>
                  <h3 class="subtitle-sections">
                    <?php the_title();?>
                  </h3>
                  
                    <?php the_content();?>
                  
                </div>
              </div>
             <?php endwhile; ?>

          </div>
        </div>
    </section>

    <section id="cases" class="padding-content opacity-back">
      <div class="uk-container uk-container-center">
        <h2 class="title-sections"><?php _e( 'Cases', 'bats' );?></h2>
      </div>
      <!-- Filter Controls -->
      <div class="container-tabs">
        <div class="uk-container uk-container-center">
          
            <?php $tags = get_tags();
            $html = '<ul id="cases" class="uk-subnav tabnav-padrao">';
            $html .= '<li class="uk-active" data-uk-filter=""><a href="">';
            $html .= __( 'Todos', 'bats' ).'</a></li>';
            foreach ( $tags as $tag ) {
              $tag_link = get_tag_link( $tag->term_id );
                  
              $html .= "<li data-uk-filter='{$tag->slug}'><a href='' title='{$tag->name}'>";
              $html .= "{$tag->name}</a></li>";
            }
            $html .= '</ul>';
            echo $html;?>
          
        </div>
      </div>

      <!-- Dynamic Grid -->
      <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-4" data-uk-grid="{controls: '#cases'}">
          
          
           <?php query_posts('category_name=cases'); ?>
            <?php while (have_posts()) : the_post(); ?>
              <a href="<?php the_permalink();?>" data-uk-filter=" 
              <?php $posttags = get_the_tags();
                if ($posttags) {
                  foreach($posttags as $tag) {
                    echo $tag->slug . ' ', ','; 
                  }
                } ?> ">
                <article class="case-box">
                  <?php the_post_thumbnail( );?>
                  <div class="mask-case">
                    <div class="container-case">
                      <h3 class="min-title">
                        <?php the_title();?>
                      </h3>
                      <?php the_excerpt();?>
                    </div>
                  </div>
                </article>
              </a>
            <?php endwhile; ?>
      </div>
    </section>

    <section id="contato" class="padding-content">
      <div class="uk-container uk-container-center">
        <h2 class="title-sections"><?php _e( 'Contato', 'bats' );?></h2>
      </div>

      <!-- This is the nav containing the toggling elements -->
      <div class="container-tabs">
        <div class="uk-container uk-container-center">
          <ul class="uk-subnav tabnav-padrao" data-uk-switcher="{connect:'#forms',animation: 'fade'}">
              <li><a href=""><?php _e( 'Contato', 'bats' );?></a></li>
              <li><a href=""><?php _e( 'Orçamento', 'bats' );?></a></li>
              <li><a href=""><?php _e( 'Vagas', 'bats' );?></a></li>
          </ul>
        </div>
      </div>

      <!-- This is the container of the content items -->
      <ul id="forms" class="uk-switcher">
          <li class="uk-container uk-container-center"> 
            <div class="uk-width-large-1-2 uk-container-center">
              <h3 class="min-title">
              <?php _e( 'Mande-nos uma mensagem ou solicite um orçamento preenchendo os campos abaixo:
              <span>Os campos marcados com “<strong> • </strong>” são de preenchimento obrigatório.</span>', 'bats' );?>
              </h3>
              <?php echo do_shortcode('[contact-form-7 id="12" title="Contato form"]');?>
            </div>
          </li>
          <li class="uk-container uk-container-center"> 
            <div class="uk-width-large-1-2 uk-container-center">
              <h3 class="min-title">
              <?php _e( 'Descreva o que precisa em poucas palavras preenchendo os campos abaixo:
              <span>Os campos marcados com “<strong> • </strong>” são de preenchimento obrigatório.</span>', 'bats' );?>
              </h3>
              <?php echo do_shortcode('[contact-form-7 id="150" title="orçamento"]');?>
            </div>
          </li>
          <li class="uk-container uk-container-center"> 
            <div class="uk-width-large-1-2 uk-container-center">
              <h3 class="min-title">
              <?php _e( 'Cadastre seus dados preenchendo os campos abaixo:
              <span>Os campos marcados com “<strong> • </strong>” são de preenchimento obrigatório.</span>', 'bats' );?>
              </h3>
              <?php echo do_shortcode('[contact-form-7 id="151" title="curriculo"]');?>
            </div>
          </li>
      </ul>
    </section>

    <section id="blog" class="padding-content opacity-back">
      <div class="uk-container uk-container-center">
        <h2 class="title-sections"><?php _e( 'Blog', 'bats' );?></h2>
        <div class="uk-grid">

          <!-- Start the Loop. -->
           <?php query_posts('category_name=blog&posts_per_page=3'); ?>
            <?php while (have_posts()) : the_post(); ?>

            <a class="uk-width-large-1-3 post-block-home" href="<?php the_permalink();?>">
              <article class="uk-grid uk-grid-small">
                <div class="uk-width-1-3">
                  <?php the_post_thumbnail('blog-thumb');?>
                </div>
                <div class="uk-width-2-3 post-list-content">
                  <h3 class="min-title">
                    <?php the_title();?>
                  </h3>
                  <date>
                    <?php the_date();?>
                  </date>
                </div>
              </article>
            </a>
           <?php endwhile; ?>

        </div>
      </div>
    </section>
<?php get_footer( );?>
