<?php get_header(); ?>

  <?php if (have_posts()) : ?>
  <section id="results" class="padding-content">
    <div class="uk-container uk-container-center">
      <h1 class="title-sections"><?php _e( 'Busca', 'bats' );?> </h1>

      <h2 class="subtitle-sections"><?php _e( 'Resultados para', 'bats' );?> <span>"<?php echo $s ?>"</span></h2>


      <?php while (have_posts()) : the_post(); ?>

        <div class="result-content" id="post-<?php the_ID(); ?>">

          <h3 class="large nomargin">
            <a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?></a>
          </h3>
          <?php
          // Support for "Search Excerpt" plugin
          // http://fucoder.com/code/search-excerpt/
          if ( function_exists('the_excerpt') && is_search() ) {
            the_excerpt();
          } ?>
          <p class="small-metas">
            <?php the_time('j F Y') ?> &nbsp;|&nbsp; 
            <!-- by <?php the_author() ?> -->
            <?php _e( 'Publicado em', 'bats' );?>
            <?php the_category(', ');
              if($post->comment_count > 0) { 
                  echo ' &nbsp;|&nbsp; ';
                  comments_popup_link('', '1 Comment', '% Comments'); 
              }
            ?>
          </p>
          
        </div>
        
        <hr>
      <?php endwhile; ?>

      <div class="navigation">
        <div class="alignleft"><?php next_posts_link('<i class="uk-icon-long-arrow-right"></i>') ?></div>
        <div class="alignright"><?php previous_posts_link('<i class="uk-icon-long-arrow-left"></i>') ?></div>
      </div>
    </div>
  </section>

  <?php else : ?>
  <section id="no-results" class="padding-content">
    <div class="uk-container uk-container-center">
      <h1 class="title-sections">
        <?php _e( 'Busca', 'bats' );?>
      </h1>
    </div>
                    <div class="uk-container uk-container-center uk-text-center">
                              
                              <h2 class="subtitle-sections">
                                <?php _e( 'Nenhum resultado para', 'bats' );?> "<?php echo $s ?>"
                                <br/><br/><i class="uk-icon-eye-slash uk-icon-large"></i>
                              </h2>
                              <p>
                                <?php _e( 'Por favor, tente uma busca diferente ou volte para home.', 'bats' );?>
                              </p>
                              <div class="">
                                <a href="<?php bloginfo('url');?>" class="uk-button uk-button-primary">
                                  <?php _e( 'Voltar para home', 'bats' );?>
                                </a>
                              </div>
                    </div>
                  
  </section>
  <?php endif; ?>

<?php get_footer(); ?>