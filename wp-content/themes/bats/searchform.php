<div id="search">
	<form class="uk-form" method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
		<div>
			<input type="text" name="s" id="s" placeholder="<?php _e('Faça sua Busca...', 'bats');?>"
			value="<?php echo wp_specialchars($search_text, 1); ?>"
			onFocus="clearInput('s', '<?php echo wp_specialchars($search_text, 1); ?>')" 
			onBlur="clearInput('s', '<?php echo wp_specialchars($search_text, 1); ?>')"
			/>
			<!-- <button type="submit"><i class="uk-icon-search"></i></button> -->
		</div>
	</form>
</div>