<?php get_header( );?>
  
  <div class="master_banner-single">
    <div class="uk-container uk-container-center"> 
      <div class="padding-content">
      <?php while (have_posts()) : the_post(); ?>
        <h1 class="title-sections">
          <?php _e( 'Blog', 'bats' );?>
        </h1>
        <h1>
          <?php the_title();?>
        </h1>
        <p class="small-metas">
            <?php the_time('j F Y') ?> &nbsp;|&nbsp; 
            <!-- by <?php the_author() ?> -->
            <?php _e( 'Publicado em', 'bats' );?>
            <?php the_category(', ');
              if($post->comment_count > 0) { 
                  echo ' &nbsp;|&nbsp; ';
                  comments_popup_link('', '1 Comment', '% Comments'); 
              }
            ?>
          </p>
      <?php endwhile; ?>
      </div>
    </div>
  </div>
  <div class="uk-container uk-container-center"> 
  <div class="uk-grid uk-grid-divider">
    <main id="blog" class="uk-width-large-3-4">
            <!-- Start the Loop. -->
            <?php while (have_posts()) : the_post(); ?>
              <article class="content-single">
                  <?php the_post_thumbnail('single-thumb');?>
                <div class="post-content">
                  <?php the_content();?>
                </div>
                <?php echo do_shortcode('[fbcomments width="100%" count="off" num="5"]'); ?>
              </article>
           <?php endwhile; ?>
    </main>
    <sidebar class="uk-width-large-1-4">
      <div class="container-sidebar">
        <?php dynamic_sidebar('blog-sidebar'); ?>
      </div>
    </sidebar>
  </div>
</div>
    
<? get_footer( );?>
