<?php get_header( );?>
  
  <div class="master_banner-empresas uk-cover-background opacity-back"> 
                    <div class="uk-container uk-container-center uk-position-relative">
                        <h1 class="uk-width-large-1-2">
                          <?php the_title(); ?>
                        </h1>
                        <p class="intro-empresa uk-width-large-1-2">
                          <span style="background: #fff;"></span>
                            <?php _e( 'Publicado em', 'bats' );?>
                            <?php the_category(', ');?>
                        </p>

                        <div class="imagem-destaque-empresa">
                          <?php the_post_thumbnail();?>
                        </div>
                    </div>
        </div>


    <section id="sobre" class="padding-content">
      <div class="uk-container uk-container-center">
        <div class="uk-grid">
        <?php while (have_posts()) : the_post(); ?>
                <div class="empresa-content uk-width-large-1-2">
                  <h2 class="title-sections"><?php _e( 'O projeto', 'bats' );?></h2>
                  <?php the_content();?>
                </div>
        <?php endwhile; ?>
        </div>
      </div>
    </section>
    
<? get_footer( );?>
