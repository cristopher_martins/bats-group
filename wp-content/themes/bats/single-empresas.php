<?php if( have_rows('paleta_de_cores') ): 
  while( have_rows('paleta_de_cores') ): the_row(); 

    $color1 = get_sub_field('cor_primaria');
    $color2 = get_sub_field('cor_secundaria');
  ?>

<?php include('header-empresas.php');?>

   <?php if( have_rows('banner_empresas') ): ?>
      
        <?php while ( have_rows('banner_empresas') ) : the_row(); ?>  
        <div class="master_banner-empresas uk-cover-background" style="background: <?php echo $color1;?>;"> 
                    <div class="uk-container uk-container-center uk-position-relative">
                        <h1 class="uk-width-large-1-2">
                          <?php the_title(); ?>
                        </h1>
                        <p class="intro-empresa uk-width-large-1-2">
                          <span style="background: <?php echo $color2;?>;"></span>
                          <?php the_sub_field('texto_introducao');?>
                        </p>

                        <div class="imagem-destaque-empresa">
                          <img src="<?php the_sub_field('imagem_destaque');?>">
                        </div>
                    </div>
        </div>
        <?php endwhile; ?>
      
    <?php endif; ?>


    <section id="sobre" class="padding-content">
      <div class="uk-container uk-container-center">
        <div class="uk-grid">
        <?php while (have_posts()) : the_post(); ?>
                <div class="empresa-content uk-width-large-1-2">
                  <h2 class="title-sections"><?php _e( 'Sobre', 'bats' );?></h2>
                  <?php the_content();?>
                </div>
        <?php endwhile; ?>
        </div>
      </div>
    </section>

  <?php if( have_rows('cases_empresa') ): ?>
    <section id="cases" class="padding-content opacity-back">
      <div class="uk-container uk-container-center">
        <h2 class="title-sections"><?php _e( 'Cases', 'bats' );?></h2>
      </div>
      

      <!-- Dynamic Grid -->
      <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-4" data-uk-grid="{controls: '#cases'}">
 
            <?php while ( have_rows('cases_empresa') ) : the_row(); ?> 

            <?php $post_object = get_sub_field('case'); ?>
 
              <?php if( $post_object ): ?>
       
              <?php $post = $post_object; setup_postdata( $post ); ?>

                <div data-uk-filter=" 
                <?php $posttags = get_the_tags();
                  if ($posttags) {
                    foreach($posttags as $tag) {
                      echo $tag->slug . ' ', ','; 
                    }
                  } ?> ">
                  <article class="case-box">
                    <?php the_post_thumbnail( );?>
                    <div class="mask-case">
                      <div class="container-case">
                        <h3 class="min-title"><?php the_title();?></h3>
                        <?php the_excerpt();?>
                      </div>
                    </div>
                  </article>
                </div>

                <?php wp_reset_postdata(); ?>
       
              <?php endif; ?>

            <?php endwhile; ?>
        
          

      </div>
    </section>
    <?php endif; ?>

    <?php endwhile; ?>
<?php endif; ?>
    
<?php include('footer-empresas.php');?>
